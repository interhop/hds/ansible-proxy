# Installation ansible

``` bash
# setup a virtualenv
virtualenv venv
source venv/bin/activate
# install ansible
pip install ansible==2.9.16
```

Openssl have to be installed for the host machin.

# Configuration environnement-

- create ```inventory``` directory and create a file conf.txt

```
[proxy]
p ansible_port=22 ansible_host=192.168.33.10 ansible_user=vagrant ansible_ssh_private_key_file=/Users/aparrot/.../vagrant/.vagrant/machines/proxy/virtualbox/private_key

[server]
ds ansible_port=22 ansible_host=192.168.33.11 ansible_user=vagrant ansible_ssh_private_key_file=/Users/aparrot/.../vagrant/.vagrant/machines/server/virtualbox/private_key
```

# Useful commands

- To set passphrass:
```ssh-add ~/.ssh/my_pubkey```

- To pass a root password
```ansible-playbook -i inventory/conf.txt --ask-become-pass playbook.yml```

- To execute a tags

```ansible-playbook -i inventory/conf.txt --ask-become-pass  --tags proxy playbook.yml```

# To do
- Install Cryptpad
  - nginx reverse proxy conf : https://github.com/xwiki-labs/cryptpad/blob/main/docs/example.nginx.conf
  - https://blog.zwindler.fr/2020/06/29/cryptpad-le-cloud-zero-knowledge/
